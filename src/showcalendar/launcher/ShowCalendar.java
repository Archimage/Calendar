/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showcalendar.launcher;

import java.util.Scanner;
import showcalendar.internal.GenerateCalendar;
/**
 *
 * @author Archimage
 */
public class ShowCalendar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.print("Enter calendar year: ");
        Scanner input = new Scanner(System.in);
        //Get user input
        int year = input.nextInt();
        new GenerateCalendar().printCalendar(year);
    }
    
}
