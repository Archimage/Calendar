/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showcalendar.internal;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

/**
 *
 * @author Archimage
 */
public class GenerateCalendar {
    //Global variables.
    private LocalDate date;
    private static final DateTimeFormatter FORMAT = 
            DateTimeFormatter.ofPattern("d/M/yyyy");
    private static final Locale LOCAL = Locale.US;
    
    public GenerateCalendar(){ }
    
    public void printCalendar(int year){
        date = LocalDate.parse("1/1/" + year, FORMAT);
        //Loop to print each month
        for(int i = 1; i <= 12; i++){
            formatPrintMonth(i, year);
            printLine();
            //Print the name of the days.
            printCalendarHeader();
            //Print the content of the current month.
            printCalendarDate(i);
            //Set month to the next month.
            date = date.plusMonths(1);
            System.out.println();
        }
    }
    
    //Formats printing to console.
    private void formatPrintMonth(int month, int year){
        Month monthName = Month.of(month);
        System.out.println(monthName.getDisplayName(TextStyle.FULL, LOCAL) 
                + year);
    }
    
    private void printLine(){
        System.out.println("-------------------------------------------------");
    }
    
    private int determineNumberOfDays(int month, LocalDate date){
        int numberOfDays = 0;
        switch(month){
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                numberOfDays = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                numberOfDays = 30;
                break;
            case 2:
                //If leap year, 29 days; otherwise, 28 days.
                numberOfDays = date.isLeapYear() ? 29 : 28;
                break;
            default:
                System.out.println("No such month exist");
                break;
        }
        return numberOfDays;
    }
    
    //Set the starting position of day 1 in each month
    private void setPosition(int tabs) {
        //No tabs required if the first day is Sunday.
        if(tabs == 7){
            return;
        }
        for(int i = 0; i < tabs; i++){
            System.out.print("\t");
        }
    }
    
    //Print the dates in the calendar
    private void printCalendarDate(int i) {
        //Set position of the first day
        int firstDay = date.getDayOfWeek().getValue();
        setPosition(firstDay);
        //Third loop to print the content of the calendar
        for(int k = 1; k <= determineNumberOfDays(i, date); k++){
                //Horizontal print
                System.out.print(k + "\t");
                //New line when the day reaches saturday.
                if((firstDay + k) % 7 == 0){
                    System.out.println();
                } 
            }
    }
    
    //Print the header of calendar
    private void printCalendarHeader(){
        //Loop to print the header of the calendar
            for(int j = 1; j <= 7; j++){
                //Print Sunday first.
                if(j == 1){
                    System.out.print(DayOfWeek.of(7)
                            .getDisplayName(TextStyle.SHORT, LOCAL)+"\t");
                }
                else{
                //Print the rest of the days.
                System.out.print(DayOfWeek.of(j-1)
                        .getDisplayName(TextStyle.SHORT, LOCAL)+"\t");
                }
            }
            //New line
            System.out.println();
    }
}
